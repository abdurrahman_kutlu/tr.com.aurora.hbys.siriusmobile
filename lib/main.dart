import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter/services.dart';
import 'package:splashscreen/splashscreen.dart';

import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/hisar_logo-01.png');
}

void main() {
  runApp(new MaterialApp(
      debugShowCheckedModeBanner: false, home: new MyGetHttpData()));
}

// Create a stateful widget
class MyGetHttpData extends StatefulWidget {
  @override
  MyGetHttpDataState createState() => new MyGetHttpDataState();
}

// Create the state for our stateful widget
class MyGetHttpDataState extends State<MyGetHttpData> {
  final String siteUrl = "https://siriusonline-dev.hisarhospital.com/#/";
  int status;

  WebviewScaffold ws = new WebviewScaffold(
      url: "https://siriusonline-dev.hisarhospital.com/#/",
      withLocalStorage: true);

//  Scaffold sf = new Scaffold(
//      body: SimpleDialog(
//    title: new Center(child: Text('Yükleniyor...')),
//    children: <Widget>[
//      new Container(
//        height: 200.0,
//        width: 200.0,
//        child: Container(
//          width: 175.0,
//            height: 175.0,
//            decoration: BoxDecoration(
//                color: Colors.white,
//                image: DecorationImage(
//                  fit: BoxFit.fitWidth,
//                  image: AssetImage('assets/hisar_logo-01.png'),
//                ))),
//      )
//    ],
//  ));

  SplashScreen ss = new SplashScreen(
      seconds: 2,
      title: new Text('Hoşgeldiniz'),
      image: new Image.asset('assets/hisar_logo-01.png'),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      loaderColor: Colors.red
  );

  Future<bool> check() async {
    var response = await http.get(Uri.encodeFull(siteUrl));
    setState(() {
      status = response.statusCode;
    });
    return true;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    check();
    if (status == 200) {
      return ws;
    } else {
      return ss;
    }
  }
}
